const express = require("express");
const getURL = require("./src/get-url.js");
const ampify = require("./src/ampify.js");

let app = express();

app.get("/", (req, res) => {
  const url = req.query.url;
  let ampSource = "";

  if (!url) res.send("ops!");

  getURL(url, function(html) {
    let ampSource = ampify(html, "https://cms-assets.getninjas.com.br/assets/");
    res.set("Content-Type", "text/html");
    res.send(ampSource);
    console.log("sended!");
  });
});

app.listen(4000, () => {
  console.log("Server is up!");
});
