const ampify = require("./src/ampify.js");

module.exports = (html, success) => {

  return ampify(html, (err, amp) => {
    if (err) throw err;
    return success(amp);
  });
}
