const http = require("https");

module.exports = (url, success) => {

  http.get(url, (response) => {
    let rawData = "";

    response.on("data", (chunk) => rawData += chunk);

    response.on("end", () => {
      try {
        return success(rawData);
      } catch (err) {
        console.log(err.message);
      }
    });
  });
};
