const ampify = require("ampify");

module.exports = (html, assetsURL) => {
  return ampify(html, {
    normalizeWhitespace: true,
    cwd: assetsURL
  });
}
